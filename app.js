(function() {
    "use strict";

    var jshint = require("jshint").JSHINT;
    var readdirp = require('readdirp');

    function lintFile(filename, options, globals) {
        var sourceCode = require("fs").readFileSync(filename, "utf8");
        var pass = jshint(sourceCode, options, globals);

        if (pass) {
            console.log(filename + " ok");
        }
        else {
            console.log("\n\n" + filename + " failed \n");
            for (var i = 0; i < jshint.errors.length; i++) {
                var error = jshint.errors[i];
                if (error) {
                    if (error.evidence) console.log(error.line + ": " + error.evidence.trim());
                    console.log("   " + error.reason);
                }
            }
        }
        return pass;
    }

    function lintFileList(filenameList, options, globals) {
        var allPass = true;
        filenameList.forEach(function(filename) {
            var pass = lintFile(filename, options, globals);
            allPass = allPass && pass;
        });
        return allPass;
    }

    var options = {
        bitwise: true,
        curly: false,
        eqeqeq: true,
        forin: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        noempty: true,
        nonew: true,
        regexp: true,
        undef: true,
        strict: true,
        trailing: true,
        node: true,
        unused:true
    };

    var globals = {
        MY_GLOBAL: true
    };

    var filenames = [];

        readdirp({ root: './', fileFilter: '*.js', directoryFilter: [ '!.git', '!*modules' ] })
        .on('data', function (entry) {

            filenames.push(entry.path);

        }).on('end', function(){

            lintFileList(filenames, options, globals);

            console.log("finished");
        });

}());

